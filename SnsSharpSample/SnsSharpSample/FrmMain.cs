﻿using SNS.SnsSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace SnsSharpSample
{
    public partial class FrmMain : Form
    {
        Random random = new Random();

        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

        }

        private void btnWriteLog_Click(object sender, EventArgs e)
        {
            SnsLog.Write("Time", DateTime.Now.ToString() + "_" + DateTime.Now.Millisecond);
        }

        private void btnWriteTiff16_Click(object sender, EventArgs e)
        {
            int imgW = 2048;
            int imgH = 2048;
            byte[] rawData = new byte[imgW * imgH * 2];
            random.NextBytes(rawData);
            TiffFile.Save(@"test.tiff", rawData, imgW, imgH);
        }

        private void btnWriteAvi_Click(object sender, EventArgs e)
        {
            //AviFile aviFile = new AviFile("test.avi", 24, CompressedEncodings.None);
            AviFile aviFile = new AviFile("test.avi", 24, CompressedEncodings.H264);
            int imgW = 2048;
            int imgH = 2048;
            byte[] imgData = new byte[imgW * imgH * 3];
            Bitmap bitmap = new Bitmap(imgW, imgH, PixelFormat.Format24bppRgb);
            for (int i = 0; i < 10; i++)
            {
                random.NextBytes(imgData);
                BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
                Marshal.Copy(imgData, 0, bmpData.Scan0, imgData.Length);
                bitmap.UnlockBits(bmpData);
                aviFile.AddVideoFrame(bitmap);
            }
            aviFile.Close();
        }

        private void btnParReadWrite_Click(object sender, EventArgs e)
        {
            FrmParReadWrite frmParReadWrite = new FrmParReadWrite();
            frmParReadWrite.Show();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            FrmConnect frmConnect = new FrmConnect();
            frmConnect.Show();
        }

        private void btnMultiTh_Click(object sender, EventArgs e)
        {
            FrmMultiTh frmMultiTh = new FrmMultiTh();
            frmMultiTh.Show();
        }

        private void btnShowImg_Click(object sender, EventArgs e)
        {
            FrmShowImg frmShowImg = new FrmShowImg();
            frmShowImg.Show();
        }

        private void btnDrawRoi_Click(object sender, EventArgs e)
        {
            FrmDrawAdjRoi frmDrawAdjRoi = new FrmDrawAdjRoi();
            frmDrawAdjRoi.Show();
        }

        private void btnHook_Click(object sender, EventArgs e)
        {
            FrmHook frmHook = new FrmHook();
            frmHook.Show();
        }

        private void btnFileDrag_Click(object sender, EventArgs e)
        {
            FrmFileDrag frmFileDrag = new FrmFileDrag();
            frmFileDrag.Show();
        }

        private void btnGrab_Click(object sender, EventArgs e)
        {
            FrmGrab frmGrab = new FrmGrab();
            frmGrab.Show();
        }

        
    }
}
