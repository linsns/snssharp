﻿
namespace SnsSharpSample
{
    partial class FrmGrab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.snsPictureBox1 = new SNS.SnsSharp.SnsPictureBox();
            this.btnStopOrContinue = new System.Windows.Forms.Button();
            this.btnGrab = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // snsPictureBox1
            // 
            this.snsPictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.snsPictureBox1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(190)))), ((int)(((byte)(40)))));
            this.snsPictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.snsPictureBox1.Image = null;
            this.snsPictureBox1.IsSawtooth = true;
            this.snsPictureBox1.IsShowThumbnail = true;
            this.snsPictureBox1.IsUseOpenGL = false;
            this.snsPictureBox1.Location = new System.Drawing.Point(0, 0);
            this.snsPictureBox1.MouseEnable = true;
            this.snsPictureBox1.Name = "snsPictureBox1";
            this.snsPictureBox1.Size = new System.Drawing.Size(800, 577);
            this.snsPictureBox1.SnsScale = 0F;
            this.snsPictureBox1.TabIndex = 0;
            this.snsPictureBox1.TextBackColor = System.Drawing.Color.Black;
            this.snsPictureBox1.TextColor = System.Drawing.Color.Yellow;
            this.snsPictureBox1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.snsPictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.snsPictureBox1_Paint);
            this.snsPictureBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.snsPictureBox1_MouseDoubleClick);
            // 
            // btnStopOrContinue
            // 
            this.btnStopOrContinue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStopOrContinue.Location = new System.Drawing.Point(279, 532);
            this.btnStopOrContinue.Name = "btnStopOrContinue";
            this.btnStopOrContinue.Size = new System.Drawing.Size(109, 33);
            this.btnStopOrContinue.TabIndex = 1;
            this.btnStopOrContinue.Text = "停止采集";
            this.btnStopOrContinue.UseVisualStyleBackColor = true;
            this.btnStopOrContinue.Click += new System.EventHandler(this.btnStopOrContinue_Click);
            // 
            // btnGrab
            // 
            this.btnGrab.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGrab.Location = new System.Drawing.Point(417, 532);
            this.btnGrab.Name = "btnGrab";
            this.btnGrab.Size = new System.Drawing.Size(113, 33);
            this.btnGrab.TabIndex = 2;
            this.btnGrab.Text = "抓取图像";
            this.btnGrab.UseVisualStyleBackColor = true;
            this.btnGrab.Click += new System.EventHandler(this.btnGrab_Click);
            // 
            // FrmGrab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 577);
            this.Controls.Add(this.btnGrab);
            this.Controls.Add(this.btnStopOrContinue);
            this.Controls.Add(this.snsPictureBox1);
            this.Name = "FrmGrab";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmGrab";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmGrab_FormClosing);
            this.Load += new System.EventHandler(this.FrmShowImg_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private SNS.SnsSharp.SnsPictureBox snsPictureBox1;
        private System.Windows.Forms.Button btnStopOrContinue;
        private System.Windows.Forms.Button btnGrab;
    }
}