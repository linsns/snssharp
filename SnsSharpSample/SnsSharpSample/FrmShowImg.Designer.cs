﻿
namespace SnsSharpSample
{
    partial class FrmShowImg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.snsPictureBox1 = new SNS.SnsSharp.SnsPictureBox();
            this.btnShow1 = new System.Windows.Forms.Button();
            this.btnShow2 = new System.Windows.Forms.Button();
            this.btnShow3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // snsPictureBox1
            // 
            this.snsPictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.snsPictureBox1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(190)))), ((int)(((byte)(40)))));
            this.snsPictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.snsPictureBox1.FinishType = SNS.SnsSharp.SnsPictureBox.FinishTypes.MouseCkick;
            this.snsPictureBox1.Image = null;
            this.snsPictureBox1.ImageMoveType = SNS.SnsSharp.SnsPictureBox.ImageMoveTypes.Left;
            this.snsPictureBox1.IsSawtooth = true;
            this.snsPictureBox1.IsShowThumbnail = true;
            this.snsPictureBox1.IsUseOpenGL = false;
            this.snsPictureBox1.Location = new System.Drawing.Point(0, 0);
            this.snsPictureBox1.MouseEnable = true;
            this.snsPictureBox1.Name = "snsPictureBox1";
            this.snsPictureBox1.Size = new System.Drawing.Size(800, 577);
            this.snsPictureBox1.SnsScale = 0F;
            this.snsPictureBox1.TabIndex = 0;
            this.snsPictureBox1.TextBackColor = System.Drawing.Color.Black;
            this.snsPictureBox1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(190)))), ((int)(((byte)(40)))));
            this.snsPictureBox1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.snsPictureBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.snsPictureBox1_MouseDoubleClick);
            // 
            // btnShow1
            // 
            this.btnShow1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnShow1.Location = new System.Drawing.Point(13, 527);
            this.btnShow1.Name = "btnShow1";
            this.btnShow1.Size = new System.Drawing.Size(80, 38);
            this.btnShow1.TabIndex = 1;
            this.btnShow1.Text = "显示方式1";
            this.btnShow1.UseVisualStyleBackColor = true;
            this.btnShow1.Click += new System.EventHandler(this.btnShow1_Click);
            // 
            // btnShow2
            // 
            this.btnShow2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnShow2.Location = new System.Drawing.Point(99, 527);
            this.btnShow2.Name = "btnShow2";
            this.btnShow2.Size = new System.Drawing.Size(80, 38);
            this.btnShow2.TabIndex = 2;
            this.btnShow2.Text = "显示方式2";
            this.btnShow2.UseVisualStyleBackColor = true;
            this.btnShow2.Click += new System.EventHandler(this.btnShow2_Click);
            // 
            // btnShow3
            // 
            this.btnShow3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnShow3.Location = new System.Drawing.Point(185, 527);
            this.btnShow3.Name = "btnShow3";
            this.btnShow3.Size = new System.Drawing.Size(80, 38);
            this.btnShow3.TabIndex = 3;
            this.btnShow3.Text = "显示方式3";
            this.btnShow3.UseVisualStyleBackColor = true;
            this.btnShow3.Click += new System.EventHandler(this.btnShow3_Click);
            // 
            // FrmShowImg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 577);
            this.Controls.Add(this.btnShow3);
            this.Controls.Add(this.btnShow2);
            this.Controls.Add(this.btnShow1);
            this.Controls.Add(this.snsPictureBox1);
            this.Name = "FrmShowImg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmShowImg";
            this.Load += new System.EventHandler(this.FrmShowImg_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private SNS.SnsSharp.SnsPictureBox snsPictureBox1;
        private System.Windows.Forms.Button btnShow1;
        private System.Windows.Forms.Button btnShow2;
        private System.Windows.Forms.Button btnShow3;
    }
}