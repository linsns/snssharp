﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SnsSharpSample
{
    public partial class FrmDrawAdjRoi : Form
    {
        Rectangle roi;
        bool isDrawAdjRoi;

        public FrmDrawAdjRoi()
        {
            InitializeComponent();
            snsPictureBox1.RoiEventHandler += snsPictureBox1_RoiEventHandler;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Bitmap bitmap = (Bitmap)Image.FromFile(@"nature.jpg");
            snsPictureBox1.Image = bitmap;
        }

        void snsPictureBox1_RoiEventHandler(Rectangle rect)
        {
            roi = rect;
            snsPictureBox1.Refresh();
            isDrawAdjRoi = false;
        }

        private bool IsInRectangle(Point p, Rectangle rect)
        {
            if (p.X >= rect.X && p.X <= rect.X + rect.Width && p.Y >= rect.Y && p.Y <= rect.Y + rect.Height)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsClickRectangle(Point p, Rectangle rect)
        {
            Rectangle rectOut = new Rectangle(rect.X - 10, rect.Y - 10, rect.Width + 20, rect.Height + 20);
            Rectangle rectIn = new Rectangle(rect.X + 10, rect.Y + 10, rect.Width - 20, rect.Height - 20);
            if (IsInRectangle(p, rectOut) && !IsInRectangle(p, rectIn))
            {
                return true;
            }
            return false;
        }

        private void btnSetAdjRoi_Click(object sender, EventArgs e)
        {
            isDrawAdjRoi = true;
            snsPictureBox1.DrawAdjRoi();
            roi = new Rectangle();
        }

        private void btnSetRoi_Click(object sender, EventArgs e)
        {
            snsPictureBox1.DrawRoi();
            roi = new Rectangle();
        }

        private void btnSetAdjRoi2_Click(object sender, EventArgs e)
        {
            roi = snsPictureBox1.DrawAdjRoiWait();
            snsPictureBox1.Refresh();
        }

        private void btnSetRoi2_Click(object sender, EventArgs e)
        {
            roi = snsPictureBox1.DrawRoiWait();
            snsPictureBox1.Refresh();
        }

        private void snsPictureBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (isDrawAdjRoi) { return; }
            Rectangle showRect = snsPictureBox1.ImgRectToShowRect(this.roi);
            bool isAdj = IsClickRectangle(e.Location, showRect);
            if (isAdj)
            {
                snsPictureBox1.DrawAdjRoi(this.roi);
                this.roi = new Rectangle();
            }
            else snsPictureBox1.Init();
        }

        private void snsPictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if(roi.Width>0&&roi.Height>0) snsPictureBox1.ShowRect(roi, false);
        }

        
    }
}
