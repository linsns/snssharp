﻿using SNS.SnsSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace SnsSharpSample
{
    public partial class FrmShowImg : Form
    {
        public FrmShowImg()
        {
            InitializeComponent();
        }

        private void FrmShowImg_Load(object sender, EventArgs e)
        {
            Bitmap bitmap = (Bitmap)Image.FromFile(@"nature.jpg");
            snsPictureBox1.Image = bitmap;
        }

        private void snsPictureBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            snsPictureBox1.Init();
        }

        private void btnShow1_Click(object sender, EventArgs e)
        {
            //使用图像方式
            Bitmap bitmap = (Bitmap)Image.FromFile(@"nature.jpg");
            snsPictureBox1.Image = bitmap; 
        }

        private void btnShow2_Click(object sender, EventArgs e)
        {
            //使用地址方式
            Bitmap bitmap = (Bitmap)Image.FromFile(@"nature2.jpg");
            BitmapData mapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
            bitmap.UnlockBits(mapData);
            snsPictureBox1.ShowImage(mapData.Scan0, bitmap.Width, bitmap.Height, bitmap.PixelFormat); 
        }

        private void btnShow3_Click(object sender, EventArgs e)
        {
            //使用字节数组方式
            Bitmap bitmap = (Bitmap)Image.FromFile(@"nature.jpg");
            BitmapData mapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
            bitmap.UnlockBits(mapData);
            byte[] imgData = new byte[mapData.Stride * mapData.Height];
            Marshal.Copy(mapData.Scan0, imgData, 0, imgData.Length);
            snsPictureBox1.ShowImage(imgData, bitmap.Width, bitmap.Height, bitmap.PixelFormat);
        }
    }
}
