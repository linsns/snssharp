﻿namespace SnsSharpSample
{
    partial class FrmConnect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtServerPort = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.rbtnSerial = new System.Windows.Forms.RadioButton();
            this.rbtnUdp = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.rbtnTcp = new System.Windows.Forms.RadioButton();
            this.txtClientRec = new System.Windows.Forms.TextBox();
            this.txtServerRec = new System.Windows.Forms.TextBox();
            this.btnClient = new System.Windows.Forms.Button();
            this.btnServer = new System.Windows.Forms.Button();
            this.txtClientPort = new System.Windows.Forms.TextBox();
            this.txtClientIp = new System.Windows.Forms.TextBox();
            this.txtServerIp = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ledClientState = new System.Windows.Forms.PictureBox();
            this.ledServerState = new System.Windows.Forms.PictureBox();
            this.labPortOrBaudrate = new System.Windows.Forms.Label();
            this.labIpOrCom = new System.Windows.Forms.Label();
            this.chkHB = new System.Windows.Forms.CheckBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledClientState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledServerState)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chkHB);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txtServerPort);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.rbtnSerial);
            this.panel2.Controls.Add(this.rbtnUdp);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.rbtnTcp);
            this.panel2.Controls.Add(this.txtClientRec);
            this.panel2.Controls.Add(this.txtServerRec);
            this.panel2.Controls.Add(this.btnClient);
            this.panel2.Controls.Add(this.btnServer);
            this.panel2.Controls.Add(this.txtClientPort);
            this.panel2.Controls.Add(this.txtClientIp);
            this.panel2.Controls.Add(this.txtServerIp);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.ledClientState);
            this.panel2.Controls.Add(this.ledServerState);
            this.panel2.Controls.Add(this.labPortOrBaudrate);
            this.panel2.Controls.Add(this.labIpOrCom);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(325, 378);
            this.panel2.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 218);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 35;
            this.label7.Text = "接收：";
            // 
            // txtServerPort
            // 
            this.txtServerPort.Location = new System.Drawing.Point(65, 139);
            this.txtServerPort.Name = "txtServerPort";
            this.txtServerPort.Size = new System.Drawing.Size(100, 21);
            this.txtServerPort.TabIndex = 22;
            this.txtServerPort.TextChanged += new System.EventHandler(this.txtServerPort_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 32;
            this.label4.Text = "通信状态：";
            // 
            // rbtnSerial
            // 
            this.rbtnSerial.AutoSize = true;
            this.rbtnSerial.Location = new System.Drawing.Point(209, 16);
            this.rbtnSerial.Name = "rbtnSerial";
            this.rbtnSerial.Size = new System.Drawing.Size(47, 16);
            this.rbtnSerial.TabIndex = 31;
            this.rbtnSerial.TabStop = true;
            this.rbtnSerial.Text = "串口";
            this.rbtnSerial.UseVisualStyleBackColor = true;
            this.rbtnSerial.CheckedChanged += new System.EventHandler(this.rbtnSerial_CheckedChanged);
            // 
            // rbtnUdp
            // 
            this.rbtnUdp.AutoSize = true;
            this.rbtnUdp.Location = new System.Drawing.Point(148, 18);
            this.rbtnUdp.Name = "rbtnUdp";
            this.rbtnUdp.Size = new System.Drawing.Size(41, 16);
            this.rbtnUdp.TabIndex = 30;
            this.rbtnUdp.TabStop = true;
            this.rbtnUdp.Text = "UDP";
            this.rbtnUdp.UseVisualStyleBackColor = true;
            this.rbtnUdp.CheckedChanged += new System.EventHandler(this.rbtnUdp_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 29;
            this.label3.Text = "通信类型：";
            // 
            // rbtnTcp
            // 
            this.rbtnTcp.AutoSize = true;
            this.rbtnTcp.Location = new System.Drawing.Point(89, 18);
            this.rbtnTcp.Name = "rbtnTcp";
            this.rbtnTcp.Size = new System.Drawing.Size(41, 16);
            this.rbtnTcp.TabIndex = 28;
            this.rbtnTcp.TabStop = true;
            this.rbtnTcp.Text = "TCP";
            this.rbtnTcp.UseVisualStyleBackColor = true;
            this.rbtnTcp.CheckedChanged += new System.EventHandler(this.rbtnTcp_CheckedChanged);
            // 
            // txtClientRec
            // 
            this.txtClientRec.Location = new System.Drawing.Point(204, 218);
            this.txtClientRec.Multiline = true;
            this.txtClientRec.Name = "txtClientRec";
            this.txtClientRec.Size = new System.Drawing.Size(100, 147);
            this.txtClientRec.TabIndex = 27;
            // 
            // txtServerRec
            // 
            this.txtServerRec.Location = new System.Drawing.Point(65, 218);
            this.txtServerRec.Multiline = true;
            this.txtServerRec.Name = "txtServerRec";
            this.txtServerRec.Size = new System.Drawing.Size(100, 147);
            this.txtServerRec.TabIndex = 26;
            // 
            // btnClient
            // 
            this.btnClient.Location = new System.Drawing.Point(204, 169);
            this.btnClient.Name = "btnClient";
            this.btnClient.Size = new System.Drawing.Size(100, 41);
            this.btnClient.TabIndex = 25;
            this.btnClient.Text = "打开";
            this.btnClient.UseVisualStyleBackColor = true;
            this.btnClient.Click += new System.EventHandler(this.btnClient_Click);
            // 
            // btnServer
            // 
            this.btnServer.Location = new System.Drawing.Point(65, 169);
            this.btnServer.Name = "btnServer";
            this.btnServer.Size = new System.Drawing.Size(100, 41);
            this.btnServer.TabIndex = 24;
            this.btnServer.Text = "打开";
            this.btnServer.UseVisualStyleBackColor = true;
            this.btnServer.Click += new System.EventHandler(this.btnServer_Click);
            // 
            // txtClientPort
            // 
            this.txtClientPort.Location = new System.Drawing.Point(204, 139);
            this.txtClientPort.Name = "txtClientPort";
            this.txtClientPort.Size = new System.Drawing.Size(100, 21);
            this.txtClientPort.TabIndex = 23;
            this.txtClientPort.TextChanged += new System.EventHandler(this.txtClientPort_TextChanged);
            // 
            // txtClientIp
            // 
            this.txtClientIp.Location = new System.Drawing.Point(204, 108);
            this.txtClientIp.Name = "txtClientIp";
            this.txtClientIp.Size = new System.Drawing.Size(100, 21);
            this.txtClientIp.TabIndex = 21;
            this.txtClientIp.TextChanged += new System.EventHandler(this.txtClientIp_TextChanged);
            // 
            // txtServerIp
            // 
            this.txtServerIp.Location = new System.Drawing.Point(65, 108);
            this.txtServerIp.Name = "txtServerIp";
            this.txtServerIp.Size = new System.Drawing.Size(100, 21);
            this.txtServerIp.TabIndex = 20;
            this.txtServerIp.TextChanged += new System.EventHandler(this.txtServerIp_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 19;
            this.label2.Text = "服务器";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(202, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 18;
            this.label1.Text = "客户端";
            // 
            // ledClientState
            // 
            this.ledClientState.BackColor = System.Drawing.Color.Red;
            this.ledClientState.Location = new System.Drawing.Point(249, 76);
            this.ledClientState.Name = "ledClientState";
            this.ledClientState.Size = new System.Drawing.Size(16, 16);
            this.ledClientState.TabIndex = 17;
            this.ledClientState.TabStop = false;
            // 
            // ledServerState
            // 
            this.ledServerState.BackColor = System.Drawing.Color.Red;
            this.ledServerState.Location = new System.Drawing.Point(134, 73);
            this.ledServerState.Name = "ledServerState";
            this.ledServerState.Size = new System.Drawing.Size(16, 16);
            this.ledServerState.TabIndex = 16;
            this.ledServerState.TabStop = false;
            // 
            // labPortOrBaudrate
            // 
            this.labPortOrBaudrate.AutoSize = true;
            this.labPortOrBaudrate.Location = new System.Drawing.Point(21, 142);
            this.labPortOrBaudrate.Name = "labPortOrBaudrate";
            this.labPortOrBaudrate.Size = new System.Drawing.Size(41, 12);
            this.labPortOrBaudrate.TabIndex = 34;
            this.labPortOrBaudrate.Text = "Port：";
            // 
            // labIpOrCom
            // 
            this.labIpOrCom.AutoSize = true;
            this.labIpOrCom.Location = new System.Drawing.Point(21, 111);
            this.labIpOrCom.Name = "labIpOrCom";
            this.labIpOrCom.Size = new System.Drawing.Size(29, 12);
            this.labIpOrCom.TabIndex = 33;
            this.labIpOrCom.Text = "IP：";
            // 
            // chkHB
            // 
            this.chkHB.AutoSize = true;
            this.chkHB.Location = new System.Drawing.Point(17, 46);
            this.chkHB.Name = "chkHB";
            this.chkHB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkHB.Size = new System.Drawing.Size(84, 16);
            this.chkHB.TabIndex = 36;
            this.chkHB.Text = "：启用心跳";
            this.chkHB.UseVisualStyleBackColor = true;
            this.chkHB.CheckedChanged += new System.EventHandler(this.chkHB_CheckedChanged);
            // 
            // FrmConnect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 406);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmConnect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmConnect";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmConnect_FormClosing);
            this.Load += new System.EventHandler(this.FrmConnect_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledClientState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledServerState)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtServerPort;
        private System.Windows.Forms.Label labPortOrBaudrate;
        private System.Windows.Forms.Label labIpOrCom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rbtnSerial;
        private System.Windows.Forms.RadioButton rbtnUdp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbtnTcp;
        private System.Windows.Forms.TextBox txtClientRec;
        private System.Windows.Forms.TextBox txtServerRec;
        private System.Windows.Forms.Button btnClient;
        private System.Windows.Forms.Button btnServer;
        private System.Windows.Forms.TextBox txtClientPort;
        private System.Windows.Forms.TextBox txtClientIp;
        private System.Windows.Forms.TextBox txtServerIp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox ledClientState;
        private System.Windows.Forms.PictureBox ledServerState;
        private System.Windows.Forms.CheckBox chkHB;
    }
}