﻿using SNS.SnsSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SnsSharpSample
{
    public partial class FrmHook : Form
    {
        SnsKeyboardHook snsKeyboardhook;
        SnsMouseHook snsMouseHook;

        public FrmHook()
        {
            InitializeComponent();
        }

        private void FrmHook_Load(object sender, EventArgs e)
        {
            snsKeyboardhook = new SnsKeyboardHook();
            snsKeyboardhook.KeyDown += SnsKeyboardhook_KeyDown; 
            snsKeyboardhook.Start();

            snsMouseHook = new SnsMouseHook();
            snsMouseHook.MouseDown += SnsMouseHook_MouseDown;
            snsMouseHook.MouseUp += SnsMouseHook_MouseUp;
            snsMouseHook.Start();
        }

        private void FrmHook_FormClosing(object sender, FormClosingEventArgs e)
        {
            snsKeyboardhook.Stop();
            snsMouseHook.Stop();
        }

        private void SnsMouseHook_MouseUp(object sender, MouseEventArgs e)
        {
            labState.Text = string.Format("状态：当前松开鼠标{0}", e.Button);
        }

        private void SnsKeyboardhook_KeyDown(object sender, KeyEventArgs e)
        {
             labState.Text = string.Format("状态：当前按下键盘{0}", e.KeyCode);
        }

        private void SnsMouseHook_MouseDown(object sender, MouseEventArgs e)
        {
            labState.Text = string.Format("状态：当前按下鼠标{0}", e.Button);
        }

        private void btnKeyboard_Click(object sender, EventArgs e)
        {
            SnsKeyboardSimulator.KeyDown(Keys.Space);
        }

        private void btnMouse_Click(object sender, EventArgs e)
        {
            for (int y = this.Location.Y+30; y < this.Location.Y+this.ClientSize.Height; y+=20)
            {
                for (int x = this.Location.X; x < this.Location.X+this.ClientSize.Width; x++)
                {
                    SnsMouseSimulator.MouseMouve(x, y);
                    Thread.Sleep(1);
                }
            }
        }
    }
}
