﻿
namespace SnsSharpSample
{
    partial class FrmHook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labState = new System.Windows.Forms.Label();
            this.btnKeyboard = new System.Windows.Forms.Button();
            this.btnMouse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labState
            // 
            this.labState.AutoSize = true;
            this.labState.Location = new System.Drawing.Point(40, 51);
            this.labState.Name = "labState";
            this.labState.Size = new System.Drawing.Size(41, 12);
            this.labState.TabIndex = 30;
            this.labState.Text = "状态：";
            // 
            // btnKeyboard
            // 
            this.btnKeyboard.Location = new System.Drawing.Point(42, 115);
            this.btnKeyboard.Name = "btnKeyboard";
            this.btnKeyboard.Size = new System.Drawing.Size(100, 41);
            this.btnKeyboard.TabIndex = 31;
            this.btnKeyboard.Text = "模拟键盘按键";
            this.btnKeyboard.UseVisualStyleBackColor = true;
            this.btnKeyboard.Click += new System.EventHandler(this.btnKeyboard_Click);
            // 
            // btnMouse
            // 
            this.btnMouse.Location = new System.Drawing.Point(172, 115);
            this.btnMouse.Name = "btnMouse";
            this.btnMouse.Size = new System.Drawing.Size(100, 41);
            this.btnMouse.TabIndex = 32;
            this.btnMouse.Text = "模拟鼠标移动";
            this.btnMouse.UseVisualStyleBackColor = true;
            this.btnMouse.Click += new System.EventHandler(this.btnMouse_Click);
            // 
            // FrmHook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(306, 182);
            this.Controls.Add(this.btnMouse);
            this.Controls.Add(this.btnKeyboard);
            this.Controls.Add(this.labState);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmHook";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmHook";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmHook_FormClosing);
            this.Load += new System.EventHandler(this.FrmHook_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labState;
        private System.Windows.Forms.Button btnKeyboard;
        private System.Windows.Forms.Button btnMouse;
    }
}