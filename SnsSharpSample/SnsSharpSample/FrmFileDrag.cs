﻿using SNS.SnsSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SnsSharpSample
{
    public partial class FrmFileDrag : Form
    {
        SnsFileDrag snsFileDrag1;
        SnsFileDrag snsFileDrag2;

        public FrmFileDrag()
        {
            InitializeComponent();
        }

        private void FrmFileDrag_Load(object sender, EventArgs e)
        {
            snsFileDrag1 = new SnsFileDrag(snsPictureBox1);
            snsFileDrag1.DragEventHandler += SnsFileDrag1_DragEventHandler;
            snsFileDrag1.Start();
            snsFileDrag2 = new SnsFileDrag(label1);
            snsFileDrag2.DragEventHandler += SnsFileDrag2_DragEventHandler; ;
            snsFileDrag2.Start();
        }

        private void SnsFileDrag1_DragEventHandler(string[] paths)
        {
            if (paths.Length>0)
            {
                label1.Visible = false;
                Bitmap bitmap = (Bitmap)Image.FromFile(paths[0]);
                snsPictureBox1.Image = bitmap;
            }
        }

        private void SnsFileDrag2_DragEventHandler(string[] paths)
        {
            if (paths.Length > 0)
            {
                label1.Visible = false;
                Bitmap bitmap = (Bitmap)Image.FromFile(paths[0]);
                snsPictureBox1.Image = bitmap;
            }
        }

        private void FrmFileDrag_FormClosing(object sender, FormClosingEventArgs e)
        {
            snsFileDrag1.Close();
            snsFileDrag2.Close();
        }
    }
}
