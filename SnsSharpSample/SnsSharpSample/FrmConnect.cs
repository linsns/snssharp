﻿using SNS.SnsSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace SnsSharpSample
{
    public partial class FrmConnect : Form
    {
        string ip;
        string port = "4000";
        string comServer="COM2";
        string comClient="COM3";
        string baudrateServer = "9600";
        string baudrateClient = "9600";
        ConnectTypes connectType = ConnectTypes.Tcp;
        IfaceConnector snsConnectorServer;
        IfaceConnector snsConnectorClient;
        SnsTimer snsTimer;
        bool isHBEnable = false;

        private enum ConnectTypes
        {
            Tcp=0,
            Udp,
            Serial,
        }

        public FrmConnect()
        {
            InitializeComponent();
        }

        private void FrmConnect_Load(object sender, EventArgs e)
        {
            snsTimer = new SnsTimer();
            snsTimer.Period = 10;
            snsTimer.TimerEventHandler += snsTimer_TimerEventHandler;
            snsTimer.Start();
            ip = GetLocalIPAddress();
            txtServerIp.Text = ip;
            txtClientIp.Text = ip;
            txtServerPort.Text = port;
            txtClientPort.Text = port;
            RadioButton[] rbtns = {rbtnTcp,rbtnUdp, rbtnSerial};
            rbtns[(int)connectType].Checked = true;
        }

        void snsTimer_TimerEventHandler()
        {
            if (snsConnectorClient != null && snsConnectorClient.IsConnected) snsConnectorClient.Send(DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString());
        }

        private string GetLocalIPAddress()
        {
            String str;
            String Result = "";
            String hostName = Dns.GetHostName();
            IPAddress[] myIP = Dns.GetHostAddresses(hostName);
            foreach (IPAddress address in myIP)
            {
                str = address.ToString();
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] >= '0' && str[i] <= '9' || str[i] == '.') Result = str;
                }
            }
            return Result;
        }

        #region 参数设置
        private void rbtnTcp_CheckedChanged(object sender, EventArgs e)
        {
            connectType = ConnectTypes.Tcp;
            labIpOrCom.Text = "Ip：";
            labPortOrBaudrate.Text = "Port：";
            txtClientIp.Enabled = false;
            txtClientPort.Enabled = false;
            txtServerIp.Text = ip;
            txtServerPort.Text = port;
            //txtClientIp.Text = ip;
            //txtClientPort.Text = port;
        }

        private void rbtnUdp_CheckedChanged(object sender, EventArgs e)
        {
            connectType = ConnectTypes.Udp;
            labIpOrCom.Text = "Ip：";
            labPortOrBaudrate.Text = "Port：";
            txtClientIp.Enabled = false;
            txtClientPort.Enabled = false;
            txtServerIp.Text = ip;
            txtServerPort.Text = port;
            //txtClientIp.Text = ip;
            //txtClientPort.Text = port;
        }

        private void rbtnSerial_CheckedChanged(object sender, EventArgs e)
        {
            connectType = ConnectTypes.Serial;
            labIpOrCom.Text = "串口号：";
            labPortOrBaudrate.Text = "波特率：";
            txtClientIp.Enabled = true;
            txtClientPort.Enabled = true;
            txtServerIp.Text = comServer;
            txtServerPort.Text = baudrateServer;
            txtClientIp.Text=comClient;
            txtClientPort.Text = baudrateClient;
        }

        private void txtServerIp_TextChanged(object sender, EventArgs e)
        {
            if (connectType==ConnectTypes.Serial)
            {
                this.comServer = txtServerIp.Text;
                //txtClientIp.Text = this.com;
            }
            else
            {
                this.ip = txtServerIp.Text;
                txtClientIp.Text = this.ip;
            }
            
        }

        private void txtServerPort_TextChanged(object sender, EventArgs e)
        {
            if (connectType == ConnectTypes.Serial)
            {
                this.baudrateServer = txtServerPort.Text;
            }
            else
            {
                this.port = txtServerPort.Text;
                txtClientPort.Text = this.port;
            }
            
        }

        private void txtClientIp_TextChanged(object sender, EventArgs e)
        {
            if (connectType == ConnectTypes.Serial)
            {
                this.comClient = txtClientIp.Text;
            }
        }

        private void txtClientPort_TextChanged(object sender, EventArgs e)
        {
            if (connectType == ConnectTypes.Serial)
            {
                this.baudrateClient = txtClientPort.Text;
            }
        }
        #endregion

        #region 服务器
        private void btnServer_Click(object sender, EventArgs e)
        {
            if (btnServer.Text == "打开")
            {
                btnServer.Text = "关闭";
                switch (connectType)
                {
                    case ConnectTypes.Tcp:
                        snsConnectorServer = new SnsSocketTcpServer(ip, port);
                        break;
                    case ConnectTypes.Udp:
                        snsConnectorServer = new SnsSocketUdpServer(ip, port);
                        break;
                    case ConnectTypes.Serial:
                        snsConnectorServer = new SnsSerialPort(comServer, baudrateServer);
                        break;
                    default:
                        break;
                }
                snsConnectorServer.ConnectEventHandler += SnsConnectorServer_ConnectEventHandler;
                snsConnectorServer.ReceiveEventHandler += SnsConnectorServer_ReceiveEventHandler;
                snsConnectorServer.HBEnable = isHBEnable;
                bool isOpen = snsConnectorServer.Open();
                if (!isOpen)
                {
                    MessageBox.Show("服务器打开失败");
                    btnServer.Text = "打开";
                }
            }
            else
            {
                snsConnectorServer.Close();
                btnServer.Text = "打开";
            }

        }

        private void SnsConnectorServer_ConnectEventHandler(bool obj)
        {
            try
            {
                ledServerState.BeginInvoke(new EventHandler(delegate
                {
                    ledServerState.BackColor = obj ? Color.Lime : Color.Red;
                }));
            }
            catch (Exception)
            { }
        }

        private void SnsConnectorServer_ReceiveEventHandler(string obj)
        {
            try
            {
                txtServerRec.BeginInvoke(new EventHandler(delegate
                {
                    txtServerRec.Text = obj;
                }));
            }
            catch (Exception)
            { }
            snsConnectorServer.Send(DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString());
        }
        #endregion

        #region 客户端
        private void btnClient_Click(object sender, EventArgs e)
        {
            if (btnClient.Text == "打开")
            {
                btnClient.Text = "关闭";
                switch (connectType)
                {
                    case ConnectTypes.Tcp:
                        snsConnectorClient = new SnsSocketTcpClient(ip, port);
                        //snsConnectorClient = new SnsSocketTcpClient("3nw920851oa2.vicp.fun", "22402");
                        break;
                    case ConnectTypes.Udp:
                        snsConnectorClient = new SnsSocketUdpClient(ip, port);
                        break;
                    case ConnectTypes.Serial:
                        snsConnectorClient = new SnsSerialPort(comClient, baudrateClient);
                        break;
                    default:
                        break;
                }

                snsConnectorClient.ConnectEventHandler += SnsConnectorClient_ConnectEventHandler;
                snsConnectorClient.ReceiveEventHandler += SnsConnectorClient_ReceiveEventHandler;
                snsConnectorClient.HBEnable = isHBEnable;
                bool isOpen = snsConnectorClient.Open();
                if (!isOpen)
                {
                    MessageBox.Show("客户端打开失败");
                    btnClient.Text = "打开";
                }
                //snsConnectorClient.Send("dd");
            }
            else
            {
                snsConnectorClient.Close();
                btnClient.Text = "打开";
            }
        }

        private void SnsConnectorClient_ConnectEventHandler(bool obj)
        {
            try
            {
                ledClientState.BeginInvoke(new EventHandler(delegate
                {
                    ledClientState.BackColor = obj ? Color.Lime : Color.Red;
                }));
            }
            catch (Exception)
            { }
        }

        private void SnsConnectorClient_ReceiveEventHandler(string obj)
        {
            try
            {
                txtClientRec.BeginInvoke(new EventHandler(delegate
                {
                    txtClientRec.Text = obj;
                }));
            }
            catch (Exception)
            { }
        }
        #endregion

        private void FrmConnect_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (snsTimer != null) snsTimer.Close();
            if (snsConnectorServer != null && snsConnectorServer.IsOpen)
            {
                snsConnectorServer.Close();
            }
            if (snsConnectorClient != null && snsConnectorClient.IsOpen)
            {
                snsConnectorClient.Close();
            }
        }

        private void chkHB_CheckedChanged(object sender, EventArgs e)
        {
            isHBEnable = chkHB.Checked;
        }
    }
}
