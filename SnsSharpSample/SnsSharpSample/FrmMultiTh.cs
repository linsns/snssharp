﻿using SNS.SnsSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnsSharpSample
{
    public partial class FrmMultiTh : Form
    {
        SnsMultiThread snsMultiTh = new SnsMultiThread();
        SnsTimer snsTimer;
        System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer();
        Random random = new Random();
        byte[] src = new byte[5120 * 4096 *4*2];
        byte[] dst = new byte[5120 * 4096 *4*2];
        int frameId = 0;
        long count;
        int thId;

        public FrmMultiTh()
        {
            InitializeComponent();
        }

        private void FrmMultiTh_Load(object sender, EventArgs e)
        {
            int maxThCount = snsMultiTh.GetMaxThCount();
            snsMultiTh.ThCount = maxThCount/2;
            snsMultiTh.MultiThEventHandler += MultiTh_MultiThEventHandler;
            snsMultiTh.Start();

            snsTimer = new SnsTimer();
            snsTimer.TimerEventHandler += SnsTimer_TimerEventHandler;
            snsTimer.Period = 5;//5ms
            snsTimer.Start();

            timer1.Interval = 100;
            timer1.Tick += Timer1_Tick;
            timer1.Start();
        }

        private void SnsTimer_TimerEventHandler()
        {
            snsMultiTh.Submit(frameId);
            frameId++;
        }

        private void MultiTh_MultiThEventHandler(long id, int thId, object objSend)
        {
            this.count = id;
            this.thId = thId;
            //Buffer.BlockCopy(src, 0, dst, 0, src.Length);
            SnsCopyApi.CopyMemory(dst, src, src.Length);
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            int usedThCount = 0;
            int totalThCount = 0;
            snsMultiTh.GetUsedThState(ref usedThCount, ref totalThCount);
            float mb= (long)src.Length / 1024.0f / 1024.0f;
            float gb = (long)src.Length * 200 / 1024.0f / 1024.0f / 1024.0f;
            label1.Text = string.Format("状态：数据量：{0:F1}GB/s  Thread数量:{1}/{2}  当前线程id:{3}  单帧大小：{4:F1}MB  当前帧数：{5}", gb, usedThCount, totalThCount, this.thId, mb, this.count);
            label1.ForeColor = usedThCount >= totalThCount ? Color.Red : Color.Black;
            string str = BitConverter.ToString(dst, 0, 2000).Replace("-", " ");
            txtShow.Text = str;

            #region 这个也可以不要，仅仅是为了有动态的效果
            //if (frameId % 1000 == 0)
            //{
            //    Task t = new Task(() =>
            //    {
            //        random.NextBytes(src);
            //    });
            //    t.Start();
            //}
            for (int i = 0; i < 2000; i++)
            {
                src[i] = (byte)random.Next(0, 255);
            }
            #endregion
        }

        private void FrmMultiTh_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();
            if (snsTimer!=null)
            {
                snsTimer.Close();
                snsTimer = null;
            }
            if (snsMultiTh != null)
            {
                snsMultiTh.Close();
                snsMultiTh = null;
            }
        }
    }
}
