﻿namespace SnsSharpSample
{
    partial class FrmDrawAdjRoi
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSetAdjRoi = new System.Windows.Forms.Button();
            this.snsPictureBox1 = new SNS.SnsSharp.SnsPictureBox();
            this.btnSetRoi = new System.Windows.Forms.Button();
            this.btnSetRoi2 = new System.Windows.Forms.Button();
            this.btnSetAdjRoi2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSetAdjRoi
            // 
            this.btnSetAdjRoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetAdjRoi.Location = new System.Drawing.Point(0, 20);
            this.btnSetAdjRoi.Name = "btnSetAdjRoi";
            this.btnSetAdjRoi.Size = new System.Drawing.Size(90, 42);
            this.btnSetAdjRoi.TabIndex = 1;
            this.btnSetAdjRoi.Text = "设置adjRoi";
            this.btnSetAdjRoi.UseVisualStyleBackColor = true;
            this.btnSetAdjRoi.Click += new System.EventHandler(this.btnSetAdjRoi_Click);
            // 
            // snsPictureBox1
            // 
            this.snsPictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.snsPictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.snsPictureBox1.Color = System.Drawing.Color.Lime;
            this.snsPictureBox1.FinishType = SNS.SnsSharp.SnsPictureBox.FinishTypes.MouseCkick;
            this.snsPictureBox1.Image = null;
            this.snsPictureBox1.ImageMoveType = SNS.SnsSharp.SnsPictureBox.ImageMoveTypes.Left;
            this.snsPictureBox1.IsSawtooth = false;
            this.snsPictureBox1.IsShowThumbnail = true;
            this.snsPictureBox1.IsUseOpenGL = false;
            this.snsPictureBox1.Location = new System.Drawing.Point(3, 3);
            this.snsPictureBox1.MouseEnable = true;
            this.snsPictureBox1.Name = "snsPictureBox1";
            this.snsPictureBox1.Size = new System.Drawing.Size(962, 754);
            this.snsPictureBox1.SnsScale = 0F;
            this.snsPictureBox1.TabIndex = 0;
            this.snsPictureBox1.TextBackColor = System.Drawing.Color.Black;
            this.snsPictureBox1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(190)))), ((int)(((byte)(40)))));
            this.snsPictureBox1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.snsPictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.snsPictureBox1_Paint);
            this.snsPictureBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.snsPictureBox1_MouseDoubleClick);
            // 
            // btnSetRoi
            // 
            this.btnSetRoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetRoi.Location = new System.Drawing.Point(0, 83);
            this.btnSetRoi.Name = "btnSetRoi";
            this.btnSetRoi.Size = new System.Drawing.Size(90, 42);
            this.btnSetRoi.TabIndex = 2;
            this.btnSetRoi.Text = "设置roi";
            this.btnSetRoi.UseVisualStyleBackColor = true;
            this.btnSetRoi.Click += new System.EventHandler(this.btnSetRoi_Click);
            // 
            // btnSetRoi2
            // 
            this.btnSetRoi2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetRoi2.Location = new System.Drawing.Point(0, 83);
            this.btnSetRoi2.Name = "btnSetRoi2";
            this.btnSetRoi2.Size = new System.Drawing.Size(90, 42);
            this.btnSetRoi2.TabIndex = 4;
            this.btnSetRoi2.Text = "设置roi2";
            this.btnSetRoi2.UseVisualStyleBackColor = true;
            this.btnSetRoi2.Click += new System.EventHandler(this.btnSetRoi2_Click);
            // 
            // btnSetAdjRoi2
            // 
            this.btnSetAdjRoi2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetAdjRoi2.Location = new System.Drawing.Point(0, 20);
            this.btnSetAdjRoi2.Name = "btnSetAdjRoi2";
            this.btnSetAdjRoi2.Size = new System.Drawing.Size(90, 42);
            this.btnSetAdjRoi2.TabIndex = 3;
            this.btnSetAdjRoi2.Text = "设置adjRoi2";
            this.btnSetAdjRoi2.UseVisualStyleBackColor = true;
            this.btnSetAdjRoi2.Click += new System.EventHandler(this.btnSetAdjRoi2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSetAdjRoi);
            this.groupBox1.Controls.Add(this.btnSetRoi);
            this.groupBox1.Location = new System.Drawing.Point(971, 182);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(90, 137);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "响应式：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSetAdjRoi2);
            this.groupBox2.Controls.Add(this.btnSetRoi2);
            this.groupBox2.Location = new System.Drawing.Point(971, 345);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(90, 135);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "等待式：";
            // 
            // Frm_DrawAdjRoi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 761);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.snsPictureBox1);
            this.Name = "Frm_DrawAdjRoi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DrawAdjRoi";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private SNS.SnsSharp.SnsPictureBox snsPictureBox1;
        private System.Windows.Forms.Button btnSetAdjRoi;
        private System.Windows.Forms.Button btnSetRoi;
        private System.Windows.Forms.Button btnSetRoi2;
        private System.Windows.Forms.Button btnSetAdjRoi2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

