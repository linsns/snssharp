﻿using SNS.SnsSharp;
using SpinnakerNET;
using SpinnakerNET.GenApi;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace SnsSharpSample
{
    class SpinnakerCamera : SnsGrab
    {
        //ManagedCameraList camList;
        ManagedSystem system;
        IManagedCamera myCam = null;
        public override bool OpenCamera(int camId = 0, string name = null)
        {
            system = new ManagedSystem();
            ManagedCameraList camList = system.GetCameras();
            myCam = (IManagedCamera)camList[camId];
            

            INodeMap nodeMapTLDevice = myCam.GetTLDeviceNodeMap();

            myCam.Init();

            INodeMap nodeMap = myCam.GetNodeMap();

            IEnum iAcquisitionMode = nodeMap.GetNode<IEnum>("AcquisitionMode");
            if (iAcquisitionMode == null || !iAcquisitionMode.IsWritable || !iAcquisitionMode.IsReadable)return false;
            IEnumEntry iAcquisitionModeContinuous = iAcquisitionMode.GetEntryByName("Continuous");
            if (iAcquisitionModeContinuous == null || !iAcquisitionModeContinuous.IsReadable)return false;
            // Set symbolic from entry node as new value for enumeration node
            iAcquisitionMode.Value = iAcquisitionModeContinuous.Symbolic;

            IEnum iExposureAuto = nodeMap.GetNode<IEnum>("ExposureAuto");
            if (iExposureAuto == null || !iExposureAuto.IsReadable || !iExposureAuto.IsWritable) return false;
            IEnumEntry iExposureAutoOff = iExposureAuto.GetEntryByName("Off");
            if (iExposureAutoOff == null || !iExposureAutoOff.IsReadable) return false;
            iExposureAuto.Value = iExposureAutoOff.Value;

            IEnum iGainAuto = nodeMap.GetNode<IEnum>("GainAuto");
            if (iGainAuto == null || !iGainAuto.IsReadable || !iGainAuto.IsWritable) return false;
            IEnumEntry iGainAutoOff = iExposureAuto.GetEntryByName("Off");
            if (iGainAutoOff == null || !iGainAutoOff.IsReadable) return false;
            iGainAuto.Value = iGainAutoOff.Value;

            IBool iFrameRateEnable = nodeMap.GetNode<IBool>("AcquisitionFrameRateEnable");
            iFrameRateEnable.Value = true;

            myCam.BeginAcquisition();

            this.IsOpen = true;
            return true;
        }

        public override void GrabOneImageData(ref IntPtr imgAddr)
        {
            try
            {
                using (IManagedImage rawImage = myCam.GetNextImage(1000))
                {
                    if (rawImage.IsIncomplete)
                    {

                    }
                    else
                    {
                        imgAddr = rawImage.DataPtr;
                    }
                }
                ;
                
            }
            catch (SpinnakerException ex)
            {


            }
            
        }

        public override void CloseCamera()
        {
            myCam.EndAcquisition();
            myCam.DeInit();
            //camList.Clear();// Clear camera list before releasing system
            system.Dispose();// Release system
        }

        protected override Bitmap TranRawToBitmap(byte[] raw, int width, int height)
        {
            throw new NotImplementedException();
        }

        public override void GetRoi(ref Size roi)
        {
            INodeMap nodeMap = myCam.GetNodeMap();
            IInteger iWidth = nodeMap.GetNode<IInteger>("Width");
            IInteger iHeight = nodeMap.GetNode<IInteger>("Height");
            roi.Width = (int)iWidth.Value;
            roi.Height = (int)iHeight.Value;
        }


    }
}
