﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using SNS.SnsSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace SnsSharpSample
{
    public class Camera : SnsGrab
    {
        private Nncam myCam = null;

        void DelegateOnDataCallback(IntPtr pData, ref Nncam.FrameInfoV2 info, bool bSnap)
        {
            if (pData == IntPtr.Zero || info.width <= 0 || info.height <= 0)
            {
                return;
            }
            SendOneImageData(pData);
        }

        public override bool OpenCamera(int camId = 0, string name = null)
        {
            try
            {
                Nncam.DeviceV2[] arr = Nncam.EnumV2();
                int numDevices = arr.Length;
                if (camId >= numDevices) return false;
                myCam = Nncam.Open(arr[camId].id);
                if (myCam.HwVersion == "" && myCam.SerialNumber == "")
                {
                    myCam.Close();
                    return false;
                }
                myCam.put_Option(Nncam.eOPTION.OPTION_RAW, 1);//0 = rgb, 1 = raw
                myCam.put_Option(Nncam.eOPTION.OPTION_BANDWIDTH, 100);
                myCam.put_Option(Nncam.eOPTION.OPTION_BITDEPTH, 0); //0 = 8 bits mode, 1 = 16 bits mode
                myCam.put_AutoExpoEnable(false);
                myCam.put_Option(Nncam.eOPTION.OPTION_SEQUENCER_EXPOTIME, 10000);//设置曝光时间10ms

                myCam.StartPushModeV3(new Nncam.DelegateDataCallbackV3(DelegateOnDataCallback), null);
                
                this.IsOpen = true;
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public override void CloseCamera()
        {
            try
            {
                if (myCam != null)
                {
                    myCam.Stop();
                    myCam.Close();
                    myCam = null;
                }
            }
            catch { }
            this.IsOpen = false;
        }

        public override void GetRoi(ref System.Drawing.Size roi)
        {
            if (!this.IsOpen) return;
            uint offsetX;
            uint offsetY;
            uint roiW = 0;
            uint roiH = 0;
            myCam.get_Roi(out offsetX, out offsetY, out roiW, out roiH);
            roi.Width = (int)roiW;
            roi.Height = (int)roiH;
        }

        protected override Bitmap TranRawToBitmap(byte[] raw, int width, int height)
        {
            Mat matBayer = new Mat(height, width, MatType.CV_8U, raw);
            Mat matRgb = new Mat(matBayer.Rows, matBayer.Cols, MatType.CV_8UC3);
            return BitmapConverter.ToBitmap(matRgb);
        }
    }
}
