﻿namespace SnsSharpSample
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.btnWriteTiff16 = new System.Windows.Forms.Button();
            this.btnParReadWrite = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnWriteLog = new System.Windows.Forms.Button();
            this.btnWriteAvi = new System.Windows.Forms.Button();
            this.btnMultiTh = new System.Windows.Forms.Button();
            this.btnHook = new System.Windows.Forms.Button();
            this.btnShowImg = new System.Windows.Forms.Button();
            this.btnFileDrag = new System.Windows.Forms.Button();
            this.btnGrab = new System.Windows.Forms.Button();
            this.btnDrawRoi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnWriteTiff16
            // 
            this.btnWriteTiff16.Location = new System.Drawing.Point(29, 92);
            this.btnWriteTiff16.Name = "btnWriteTiff16";
            this.btnWriteTiff16.Size = new System.Drawing.Size(117, 46);
            this.btnWriteTiff16.TabIndex = 0;
            this.btnWriteTiff16.Text = "生成16位Tiff";
            this.btnWriteTiff16.UseVisualStyleBackColor = true;
            this.btnWriteTiff16.Click += new System.EventHandler(this.btnWriteTiff16_Click);
            // 
            // btnParReadWrite
            // 
            this.btnParReadWrite.Location = new System.Drawing.Point(29, 223);
            this.btnParReadWrite.Name = "btnParReadWrite";
            this.btnParReadWrite.Size = new System.Drawing.Size(117, 46);
            this.btnParReadWrite.TabIndex = 1;
            this.btnParReadWrite.Text = "参数保存和读取";
            this.btnParReadWrite.UseVisualStyleBackColor = true;
            this.btnParReadWrite.Click += new System.EventHandler(this.btnParReadWrite_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(29, 290);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(117, 46);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.Text = "串口和网口通信";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnWriteLog
            // 
            this.btnWriteLog.Location = new System.Drawing.Point(29, 27);
            this.btnWriteLog.Name = "btnWriteLog";
            this.btnWriteLog.Size = new System.Drawing.Size(117, 46);
            this.btnWriteLog.TabIndex = 3;
            this.btnWriteLog.Text = "生成Log文件";
            this.btnWriteLog.UseVisualStyleBackColor = true;
            this.btnWriteLog.Click += new System.EventHandler(this.btnWriteLog_Click);
            // 
            // btnWriteAvi
            // 
            this.btnWriteAvi.Location = new System.Drawing.Point(29, 157);
            this.btnWriteAvi.Name = "btnWriteAvi";
            this.btnWriteAvi.Size = new System.Drawing.Size(117, 46);
            this.btnWriteAvi.TabIndex = 4;
            this.btnWriteAvi.Text = "生成Avi视频";
            this.btnWriteAvi.UseVisualStyleBackColor = true;
            this.btnWriteAvi.Click += new System.EventHandler(this.btnWriteAvi_Click);
            // 
            // btnMultiTh
            // 
            this.btnMultiTh.Location = new System.Drawing.Point(29, 357);
            this.btnMultiTh.Name = "btnMultiTh";
            this.btnMultiTh.Size = new System.Drawing.Size(117, 46);
            this.btnMultiTh.TabIndex = 5;
            this.btnMultiTh.Text = "多线程并发处理";
            this.btnMultiTh.UseVisualStyleBackColor = true;
            this.btnMultiTh.Click += new System.EventHandler(this.btnMultiTh_Click);
            // 
            // btnHook
            // 
            this.btnHook.Location = new System.Drawing.Point(174, 157);
            this.btnHook.Name = "btnHook";
            this.btnHook.Size = new System.Drawing.Size(117, 46);
            this.btnHook.TabIndex = 6;
            this.btnHook.Text = "全局热键监测";
            this.btnHook.UseVisualStyleBackColor = true;
            this.btnHook.Click += new System.EventHandler(this.btnHook_Click);
            // 
            // btnShowImg
            // 
            this.btnShowImg.Location = new System.Drawing.Point(174, 27);
            this.btnShowImg.Name = "btnShowImg";
            this.btnShowImg.Size = new System.Drawing.Size(117, 46);
            this.btnShowImg.TabIndex = 7;
            this.btnShowImg.Text = "图像显示";
            this.btnShowImg.UseVisualStyleBackColor = true;
            this.btnShowImg.Click += new System.EventHandler(this.btnShowImg_Click);
            // 
            // btnFileDrag
            // 
            this.btnFileDrag.Location = new System.Drawing.Point(174, 222);
            this.btnFileDrag.Name = "btnFileDrag";
            this.btnFileDrag.Size = new System.Drawing.Size(117, 46);
            this.btnFileDrag.TabIndex = 8;
            this.btnFileDrag.Text = "文件拖拽";
            this.btnFileDrag.UseVisualStyleBackColor = true;
            this.btnFileDrag.Click += new System.EventHandler(this.btnFileDrag_Click);
            // 
            // btnGrab
            // 
            this.btnGrab.Location = new System.Drawing.Point(174, 288);
            this.btnGrab.Name = "btnGrab";
            this.btnGrab.Size = new System.Drawing.Size(117, 46);
            this.btnGrab.TabIndex = 9;
            this.btnGrab.Text = "相机采集";
            this.btnGrab.UseVisualStyleBackColor = true;
            this.btnGrab.Click += new System.EventHandler(this.btnGrab_Click);
            // 
            // btnDrawRoi
            // 
            this.btnDrawRoi.Location = new System.Drawing.Point(174, 92);
            this.btnDrawRoi.Name = "btnDrawRoi";
            this.btnDrawRoi.Size = new System.Drawing.Size(117, 46);
            this.btnDrawRoi.TabIndex = 10;
            this.btnDrawRoi.Text = "绘制Roi";
            this.btnDrawRoi.UseVisualStyleBackColor = true;
            this.btnDrawRoi.Click += new System.EventHandler(this.btnDrawRoi_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 449);
            this.Controls.Add(this.btnDrawRoi);
            this.Controls.Add(this.btnGrab);
            this.Controls.Add(this.btnFileDrag);
            this.Controls.Add(this.btnShowImg);
            this.Controls.Add(this.btnHook);
            this.Controls.Add(this.btnMultiTh);
            this.Controls.Add(this.btnWriteAvi);
            this.Controls.Add(this.btnWriteLog);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.btnParReadWrite);
            this.Controls.Add(this.btnWriteTiff16);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SnsSharpSample";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnWriteTiff16;
        private System.Windows.Forms.Button btnParReadWrite;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnWriteLog;
        private System.Windows.Forms.Button btnWriteAvi;
        private System.Windows.Forms.Button btnMultiTh;
        private System.Windows.Forms.Button btnHook;
        private System.Windows.Forms.Button btnShowImg;
        private System.Windows.Forms.Button btnFileDrag;
        private System.Windows.Forms.Button btnGrab;
        private System.Windows.Forms.Button btnDrawRoi;
    }
}

