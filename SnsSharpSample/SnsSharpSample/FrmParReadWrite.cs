﻿using SNS.SnsSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace SnsSharpSample
{
    public partial class FrmParReadWrite : Form
    {
        Cat cat = new Cat();

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public class Cat
        {
            [MarshalAsAttribute(UnmanagedType.BStr, SizeConst = 40)]
            public string breed;//品种

            public int age;//年龄

            [MarshalAsAttribute(UnmanagedType.BStr, SizeConst = 40)]
            public string color;//颜色
        }

        public FrmParReadWrite()
        {
            InitializeComponent();
        }

        private void FrmParReadWrite_Load(object sender, EventArgs e)
        {
            cat.breed = "狸花猫";
            cat.age = 2;
            cat.color = "黑色";

            ParToUI();
        }

        private void ParToUI()
        {
            txtBreed.Text = cat.breed;
            txtAge.Text = cat.age.ToString();
            txtColor.Text = cat.color;
        }


        #region 读写Txt
        private void btnWriteTxt_Click(object sender, EventArgs e)
        {
            if (File.Exists("cat.txt")) File.Delete("cat.txt");
            TxtFile txtFile = new TxtFile("cat.txt");
            txtFile.WriteAtLastRow(cat.breed);
            txtFile.WriteAtLastRow(cat.age.ToString());
            txtFile.WriteAtLastRow(cat.color);
            txtFile.Close();
        }

        private void btnReadTxt_Click(object sender, EventArgs e)
        {
            TxtFile txtFile = new TxtFile("cat.txt");
            if (txtFile.GetRowCount()>=3)
            {
                cat.breed = txtFile.ReadAtIndexRow(0);
                cat.age = int.Parse(txtFile.ReadAtIndexRow(1));
                cat.color = txtFile.ReadAtIndexRow(2);
            }
            txtFile.Close();
            ParToUI();
        }
        #endregion

        #region 读写xml
        private void btnWriteXml_Click(object sender, EventArgs e)
        {
            XmlFile.Write("cat.xml", cat);
        }

        private void btnReadXml_Click(object sender, EventArgs e)
        {
            cat = XmlFile.Read<Cat>("cat.xml");
            ParToUI();
        }
        #endregion

        #region 读写json
        private void btnWriteJson_Click(object sender, EventArgs e)
        {
            JsonFile.Write("cat.json", cat);
        }

        private void btnReadJson_Click(object sender, EventArgs e)
        {
            cat = JsonFile.Read<Cat>("cat.json");
            ParToUI();
        }
        #endregion

        #region 读写ini
        private void btnWriteIni_Click(object sender, EventArgs e)
        {
            IniFile iniFile = new IniFile("cat.ini");
            iniFile.WriteIniKeys("cat", "breed", cat.breed);
            iniFile.WriteIniKeys("cat", "age", cat.age.ToString());
            iniFile.WriteIniKeys("cat", "color", cat.color);
        }

        private void btnReadIni_Click(object sender, EventArgs e)
        {
            IniFile iniFile = new IniFile("cat.ini");
            cat.breed = iniFile.ReadIniKeys("cat", "breed");
            cat.age = int.Parse(iniFile.ReadIniKeys("cat", "age"));
            cat.color = iniFile.ReadIniKeys("cat", "color");
            ParToUI();
        }
        #endregion

        #region 读写csv
        private void btnWriteCsv_Click(object sender, EventArgs e)
        {
            CsvFile csvFile = new CsvFile(@"cat.csv");
            csvFile.WriteAtLastRow(new string[] { "breed", "age", "color"});
            csvFile.WriteAtLastRow(new string[] { cat.breed, cat.age.ToString(), cat.color });
            csvFile.Close();
        }

        private void btnReadCsv_Click(object sender, EventArgs e)
        {
            CsvFile csvFile = new CsvFile(@"cat.csv");
            string[][] strData = csvFile.ReadAllRow();
            csvFile.Close();
        }
        #endregion

        #region 读写bin
        private void btnWriteBin_Click(object sender, EventArgs e)
        {
            BinFile binFile = new BinFile("cat.bin");
            byte[] data = SnsStructConverter.StructToBytes(cat);
            binFile.Write(data);
            binFile.Close();
        }

        private void btnReadBin_Click(object sender, EventArgs e)
        {
            BinFile binFile = new BinFile("cat.bin");
            byte[] data = binFile.ReadAll();
            cat = (Cat)SnsStructConverter.BytesToStruct(data, typeof(Cat));
            binFile.Close();
            ParToUI();
        }
        #endregion

    }
}
