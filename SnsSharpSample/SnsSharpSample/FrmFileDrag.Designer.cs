﻿
namespace SnsSharpSample
{
    partial class FrmFileDrag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.snsPictureBox1 = new SNS.SnsSharp.SnsPictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // snsPictureBox1
            // 
            this.snsPictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.snsPictureBox1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(190)))), ((int)(((byte)(40)))));
            this.snsPictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.snsPictureBox1.Image = null;
            this.snsPictureBox1.IsSawtooth = false;
            this.snsPictureBox1.IsShowThumbnail = true;
            this.snsPictureBox1.IsUseOpenGL = false;
            this.snsPictureBox1.Location = new System.Drawing.Point(0, 0);
            this.snsPictureBox1.MouseEnable = true;
            this.snsPictureBox1.Name = "snsPictureBox1";
            this.snsPictureBox1.Size = new System.Drawing.Size(800, 577);
            this.snsPictureBox1.SnsScale = 0F;
            this.snsPictureBox1.TabIndex = 1;
            this.snsPictureBox1.TextBackColor = System.Drawing.Color.Black;
            this.snsPictureBox1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(190)))), ((int)(((byte)(40)))));
            this.snsPictureBox1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(235, 271);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(330, 35);
            this.label1.TabIndex = 2;
            this.label1.Text = "将图片拖拽到此区域";
            // 
            // FrmFileDrag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 577);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.snsPictureBox1);
            this.Name = "FrmFileDrag";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmFileDrag";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmFileDrag_FormClosing);
            this.Load += new System.EventHandler(this.FrmFileDrag_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SNS.SnsSharp.SnsPictureBox snsPictureBox1;
        private System.Windows.Forms.Label label1;
    }
}