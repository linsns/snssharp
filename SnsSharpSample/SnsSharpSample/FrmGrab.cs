﻿using OpenCvSharp;
using SNS.SnsSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnsSharpSample
{
    public partial class FrmGrab : Form
    {
        SpinnakerCamera cam;
        public FrmGrab()
        {
            InitializeComponent();
        }

        private void FrmShowImg_Load(object sender, EventArgs e)
        {
            //Task task = new Task(delegate
            //{
            //    cam = new SpinnakerCamera();
            //    cam.IsColor = true;
            //    cam.BayerType = SnsBayerTypes.BayerBGGR2BGR;
            //    cam.DataGrabEventHandler += Cam_DataGrabEventHandler;
            //    cam.OpenCamera(0);
            //    cam.Start();
            //    cam.ContinousGrab();
            //});
            //task.Start();
        }

        private void FrmGrab_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cam != null)
            {
                cam.StopGrab();
                cam.Stop();
                cam.CloseCamera();
                cam = null;
            }
        }

        private void Cam_DataGrabEventHandler(IntPtr intPtr, int width, int height, SnsBayerTypes bayerType)
        {
            Mat matBayer = new Mat(height, width, MatType.CV_8U, intPtr);
            Mat matRgb = new Mat(matBayer.Rows, matBayer.Cols, MatType.CV_8UC3);
            switch (bayerType)
            {
                case SnsBayerTypes.GRAY:
                    Cv2.CvtColor(matBayer, matRgb, ColorConversionCodes.GRAY2RGB);
                    break;
                case SnsBayerTypes.BayerBGGR2BGR:
                    Cv2.CvtColor(matBayer, matRgb, ColorConversionCodes.BayerBG2BGR);//46
                    break;
                case SnsBayerTypes.BayerGBRG2BGR:
                    Cv2.CvtColor(matBayer, matRgb, ColorConversionCodes.BayerGB2BGR);//47
                    break;
                case SnsBayerTypes.BayerGRBG2BGR:
                    Cv2.CvtColor(matBayer, matRgb, ColorConversionCodes.BayerGR2BGR);//49
                    break;
                case SnsBayerTypes.BayerRGGB2BGR:
                    Cv2.CvtColor(matBayer, matRgb, ColorConversionCodes.BayerRG2BGR);//48
                    break;
                default:
                    Cv2.CvtColor(matBayer, matRgb, ColorConversionCodes.GRAY2RGB);
                    break;
            }
            snsPictureBox1.ShowImage(matRgb.Data, matRgb.Width, matRgb.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
        }

        private void snsPictureBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            snsPictureBox1.Init();
        }

        private void snsPictureBox1_Paint(object sender, PaintEventArgs e)
        {
            string strTime = "";
            strTime += string.Format("Size:{0}x{1}", snsPictureBox1.ImgWidth, snsPictureBox1.ImgHeight);
            strTime += string.Format("     Scale:{0:F2}", snsPictureBox1.SnsScale);
            strTime += string.Format("     ShowFps:{0:F1}", snsPictureBox1.ShowFps);
            snsPictureBox1.ShowString(strTime, new PointF(snsPictureBox1.XSpace + 1, snsPictureBox1.YSpace + 1), false);
        }

        private void btnStopOrContinue_Click(object sender, EventArgs e)
        {
            if (btnStopOrContinue.Text == "停止采集")
            {
                btnStopOrContinue.Text = "继续采集";
                cam.StopGrab();
            }
            else
            {
                btnStopOrContinue.Text = "停止采集";
                cam.ContinousGrab();
            }
        }

        private void btnGrab_Click(object sender, EventArgs e)
        {
            Bitmap bitmap;
            cam.SingleGrab(out bitmap);
        }
    }
}
