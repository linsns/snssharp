﻿namespace SnsSharpSample
{
    partial class FrmParReadWrite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtBreed = new System.Windows.Forms.TextBox();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtColor = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnWriteTxt = new System.Windows.Forms.Button();
            this.btnWriteXml = new System.Windows.Forms.Button();
            this.btnWriteIni = new System.Windows.Forms.Button();
            this.btnWriteBin = new System.Windows.Forms.Button();
            this.btnReadBin = new System.Windows.Forms.Button();
            this.btnReadIni = new System.Windows.Forms.Button();
            this.btnReadXml = new System.Windows.Forms.Button();
            this.btnReadTxt = new System.Windows.Forms.Button();
            this.btnReadJson = new System.Windows.Forms.Button();
            this.btnWriteJson = new System.Windows.Forms.Button();
            this.btnReadCsv = new System.Windows.Forms.Button();
            this.btnWriteCsv = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(131, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "品种：";
            // 
            // txtBreed
            // 
            this.txtBreed.Location = new System.Drawing.Point(167, 31);
            this.txtBreed.Name = "txtBreed";
            this.txtBreed.Size = new System.Drawing.Size(80, 21);
            this.txtBreed.TabIndex = 1;
            // 
            // txtAge
            // 
            this.txtAge.Location = new System.Drawing.Point(167, 71);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(80, 21);
            this.txtAge.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(131, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "年龄：";
            // 
            // txtColor
            // 
            this.txtColor.Location = new System.Drawing.Point(167, 113);
            this.txtColor.Name = "txtColor";
            this.txtColor.Size = new System.Drawing.Size(80, 21);
            this.txtColor.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(131, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "颜色：";
            // 
            // btnWriteTxt
            // 
            this.btnWriteTxt.Location = new System.Drawing.Point(64, 151);
            this.btnWriteTxt.Name = "btnWriteTxt";
            this.btnWriteTxt.Size = new System.Drawing.Size(117, 46);
            this.btnWriteTxt.TabIndex = 6;
            this.btnWriteTxt.Text = "生成Txt文件";
            this.btnWriteTxt.UseVisualStyleBackColor = true;
            this.btnWriteTxt.Click += new System.EventHandler(this.btnWriteTxt_Click);
            // 
            // btnWriteXml
            // 
            this.btnWriteXml.Location = new System.Drawing.Point(64, 203);
            this.btnWriteXml.Name = "btnWriteXml";
            this.btnWriteXml.Size = new System.Drawing.Size(117, 46);
            this.btnWriteXml.TabIndex = 7;
            this.btnWriteXml.Text = "生成Xml文件";
            this.btnWriteXml.UseVisualStyleBackColor = true;
            this.btnWriteXml.Click += new System.EventHandler(this.btnWriteXml_Click);
            // 
            // btnWriteIni
            // 
            this.btnWriteIni.Location = new System.Drawing.Point(64, 307);
            this.btnWriteIni.Name = "btnWriteIni";
            this.btnWriteIni.Size = new System.Drawing.Size(117, 46);
            this.btnWriteIni.TabIndex = 8;
            this.btnWriteIni.Text = "生成Ini文件";
            this.btnWriteIni.UseVisualStyleBackColor = true;
            this.btnWriteIni.Click += new System.EventHandler(this.btnWriteIni_Click);
            // 
            // btnWriteBin
            // 
            this.btnWriteBin.Location = new System.Drawing.Point(64, 411);
            this.btnWriteBin.Name = "btnWriteBin";
            this.btnWriteBin.Size = new System.Drawing.Size(117, 46);
            this.btnWriteBin.TabIndex = 9;
            this.btnWriteBin.Text = "生成Bin文件";
            this.btnWriteBin.UseVisualStyleBackColor = true;
            this.btnWriteBin.Click += new System.EventHandler(this.btnWriteBin_Click);
            // 
            // btnReadBin
            // 
            this.btnReadBin.Location = new System.Drawing.Point(207, 411);
            this.btnReadBin.Name = "btnReadBin";
            this.btnReadBin.Size = new System.Drawing.Size(117, 46);
            this.btnReadBin.TabIndex = 13;
            this.btnReadBin.Text = "读取Bin文件";
            this.btnReadBin.UseVisualStyleBackColor = true;
            this.btnReadBin.Click += new System.EventHandler(this.btnReadBin_Click);
            // 
            // btnReadIni
            // 
            this.btnReadIni.Location = new System.Drawing.Point(207, 307);
            this.btnReadIni.Name = "btnReadIni";
            this.btnReadIni.Size = new System.Drawing.Size(117, 46);
            this.btnReadIni.TabIndex = 12;
            this.btnReadIni.Text = "读取Ini文件";
            this.btnReadIni.UseVisualStyleBackColor = true;
            this.btnReadIni.Click += new System.EventHandler(this.btnReadIni_Click);
            // 
            // btnReadXml
            // 
            this.btnReadXml.Location = new System.Drawing.Point(207, 203);
            this.btnReadXml.Name = "btnReadXml";
            this.btnReadXml.Size = new System.Drawing.Size(117, 46);
            this.btnReadXml.TabIndex = 11;
            this.btnReadXml.Text = "读取Xml文件";
            this.btnReadXml.UseVisualStyleBackColor = true;
            this.btnReadXml.Click += new System.EventHandler(this.btnReadXml_Click);
            // 
            // btnReadTxt
            // 
            this.btnReadTxt.Location = new System.Drawing.Point(207, 151);
            this.btnReadTxt.Name = "btnReadTxt";
            this.btnReadTxt.Size = new System.Drawing.Size(117, 46);
            this.btnReadTxt.TabIndex = 10;
            this.btnReadTxt.Text = "读取Txt文件";
            this.btnReadTxt.UseVisualStyleBackColor = true;
            this.btnReadTxt.Click += new System.EventHandler(this.btnReadTxt_Click);
            // 
            // btnReadJson
            // 
            this.btnReadJson.Location = new System.Drawing.Point(207, 255);
            this.btnReadJson.Name = "btnReadJson";
            this.btnReadJson.Size = new System.Drawing.Size(117, 46);
            this.btnReadJson.TabIndex = 15;
            this.btnReadJson.Text = "读取Json文件";
            this.btnReadJson.UseVisualStyleBackColor = true;
            this.btnReadJson.Click += new System.EventHandler(this.btnReadJson_Click);
            // 
            // btnWriteJson
            // 
            this.btnWriteJson.Location = new System.Drawing.Point(64, 255);
            this.btnWriteJson.Name = "btnWriteJson";
            this.btnWriteJson.Size = new System.Drawing.Size(117, 46);
            this.btnWriteJson.TabIndex = 14;
            this.btnWriteJson.Text = "生成Json文件";
            this.btnWriteJson.UseVisualStyleBackColor = true;
            this.btnWriteJson.Click += new System.EventHandler(this.btnWriteJson_Click);
            // 
            // btnReadCsv
            // 
            this.btnReadCsv.Location = new System.Drawing.Point(207, 359);
            this.btnReadCsv.Name = "btnReadCsv";
            this.btnReadCsv.Size = new System.Drawing.Size(117, 46);
            this.btnReadCsv.TabIndex = 17;
            this.btnReadCsv.Text = "读取Csv文件";
            this.btnReadCsv.UseVisualStyleBackColor = true;
            this.btnReadCsv.Click += new System.EventHandler(this.btnReadCsv_Click);
            // 
            // btnWriteCsv
            // 
            this.btnWriteCsv.Location = new System.Drawing.Point(64, 359);
            this.btnWriteCsv.Name = "btnWriteCsv";
            this.btnWriteCsv.Size = new System.Drawing.Size(117, 46);
            this.btnWriteCsv.TabIndex = 16;
            this.btnWriteCsv.Text = "生成Csv文件";
            this.btnWriteCsv.UseVisualStyleBackColor = true;
            this.btnWriteCsv.Click += new System.EventHandler(this.btnWriteCsv_Click);
            // 
            // FrmParReadWrite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 476);
            this.Controls.Add(this.btnReadCsv);
            this.Controls.Add(this.btnWriteCsv);
            this.Controls.Add(this.btnReadJson);
            this.Controls.Add(this.btnWriteJson);
            this.Controls.Add(this.btnReadBin);
            this.Controls.Add(this.btnReadIni);
            this.Controls.Add(this.btnReadXml);
            this.Controls.Add(this.btnReadTxt);
            this.Controls.Add(this.btnWriteBin);
            this.Controls.Add(this.btnWriteIni);
            this.Controls.Add(this.btnWriteXml);
            this.Controls.Add(this.btnWriteTxt);
            this.Controls.Add(this.txtColor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtAge);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBreed);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmParReadWrite";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmParReadWrite";
            this.Load += new System.EventHandler(this.FrmParReadWrite_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBreed;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtColor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnWriteTxt;
        private System.Windows.Forms.Button btnWriteXml;
        private System.Windows.Forms.Button btnWriteIni;
        private System.Windows.Forms.Button btnWriteBin;
        private System.Windows.Forms.Button btnReadBin;
        private System.Windows.Forms.Button btnReadIni;
        private System.Windows.Forms.Button btnReadXml;
        private System.Windows.Forms.Button btnReadTxt;
        private System.Windows.Forms.Button btnReadJson;
        private System.Windows.Forms.Button btnWriteJson;
        private System.Windows.Forms.Button btnReadCsv;
        private System.Windows.Forms.Button btnWriteCsv;
    }
}